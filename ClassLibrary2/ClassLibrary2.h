﻿// ClassLibrary2.h

#pragma once
#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "Advapi32.lib")
#include <stdio.h>
#include <tchar.h>
//#undef _M_CEE;
#include <windows.h>
#include <Wincrypt.h>
#include <vector>
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

using namespace System;
using namespace System::Runtime::InteropServices;
namespace Kupcov {

	namespace WinCryptoApi {

		public ref class C3DES
		{
		private:

			static	LPTSTR Base64Decode(LPCTSTR lpData, DWORD dwSize)
			{
				DWORD dwResult = 0;
				if (CryptStringToBinary(lpData, dwSize, CRYPT_STRING_BASE64, NULL, &dwResult, NULL, NULL))
				{
					LPTSTR lpszBase64Decoded = new TCHAR[dwResult + (sizeof(TCHAR) * 2)];
					memset(lpszBase64Decoded, 0, dwResult);
					if (CryptStringToBinary(lpData, dwSize, CRYPT_STRING_BASE64, (BYTE *)lpszBase64Decoded, &dwResult, NULL, NULL))
					{
						*(LPWORD)(lpszBase64Decoded + (dwResult / sizeof(TCHAR))) = 0;

						return lpszBase64Decoded;
					}
				}
				return NULL;
			}

			static TCHAR* Base64Encode(BYTE* lpData, DWORD dwSize, DWORD *dwResult)
			{
				*dwResult = 0;
				if (CryptBinaryToString(lpData, dwSize, CRYPT_STRING_BASE64, NULL, dwResult))
				{
					TCHAR* lpszBase64 = new TCHAR[*dwResult];
					if (CryptBinaryToString(lpData, dwSize, CRYPT_STRING_BASE64, lpszBase64, dwResult))
					{
						TCHAR pByteLF = *(LPWORD)(lpszBase64 + *dwResult - 1);
						TCHAR pByteCR = *(LPWORD)(lpszBase64 + *dwResult - 2);
						if (pByteCR == 0x0D && pByteLF == 0x0A)
						{
							*(LPWORD)(lpszBase64 + *dwResult - 2) = 0;
						}
						return lpszBase64;
					}
				}
				return NULL;
			}
			static BYTE* GetHash(BYTE *passwd, DWORD pwlen, DWORD *resultlen) {
				HCRYPTPROV hProv;
				HCRYPTHASH hHash;
				if (CryptAcquireContext(&hProv, NULL, MS_STRONG_PROV, PROV_RSA_FULL, 0) == TRUE) {
					if (CryptCreateHash(hProv, CALG_SHA, 0, 0, &hHash) == TRUE) {
						if (CryptHashData(hHash, passwd, pwlen, 0) == TRUE) {
					
							if (CryptGetHashParam(hHash, HP_HASHVAL, NULL, resultlen, 0)) {
								BYTE * str = new BYTE[*resultlen +1];

								if (CryptGetHashParam(hHash, HP_HASHVAL, str, resultlen, 0)) {
									return str;
								}
							}
						}
					}
				}
			}

			static	DWORD TripleDESencrypt(BYTE *plaintext, DWORD ptlen, BYTE *passwd, DWORD pwlen, BYTE *cyphertext, DWORD *ctlen, BOOL isFinish)
			{
				HCRYPTPROV hProv;
				HCRYPTHASH hHash;
				HCRYPTKEY hKey;
				DWORD dwSizeNeeded = 0;
				DWORD sz = 0;
				if (CryptAcquireContext(&hProv, NULL, MS_STRONG_PROV, PROV_RSA_FULL, 0) == TRUE) {
					if (CryptCreateHash(hProv, CALG_SHA, 0, 0, &hHash) == TRUE) {
						if (CryptHashData(hHash, passwd, pwlen, 0) == TRUE) {
							if (CryptDeriveKey(hProv, CALG_3DES, hHash, 0, &hKey) == TRUE) {
								DWORD dwMode = CRYPT_MODE_ECB;
								if (TRUE == CryptSetKeyParam(hKey, KP_MODE, (BYTE*)&dwMode, 0))
								{
									if (NULL != cyphertext && 0 != ptlen)
									{
										memcpy(cyphertext, plaintext, ptlen);
										sz = *ctlen;
										*ctlen = ptlen;
									}
									if (FALSE == CryptEncrypt(hKey, NULL, isFinish, 0, cyphertext, ctlen, sz))
									{
										DWORD dwError = GetLastError();
										if (ERROR_MORE_DATA == dwError)
										{
											dwSizeNeeded = *ctlen * sizeof(BYTE);
										}
									}
								}
							}
						}
					}
				}
			
				CryptDestroyKey(hKey);
				CryptDestroyHash(hHash);
				CryptReleaseContext(hProv, 0);
				return dwSizeNeeded;
			}

			static	DWORD TripleDESdecrypt(BYTE *cyphertext, DWORD ctlen, BYTE *passwd, DWORD pwlen, BYTE *plaintext, DWORD *ptlen, BOOL isFinish)
			{
				HCRYPTPROV hProv;
				HCRYPTHASH hHash;
				HCRYPTKEY hKey;
				DWORD dwSizeNeeded = 0;

				if (CryptAcquireContext(&hProv, NULL, MS_STRONG_PROV, PROV_RSA_FULL, 0) == TRUE) {
					if (CryptCreateHash(hProv, CALG_SHA, 0, 0, &hHash) == TRUE) {
						if (CryptHashData(hHash, passwd, pwlen, 0) == TRUE) {
							if (CryptDeriveKey(hProv, CALG_3DES, hHash, 0, &hKey) == TRUE) {
								DWORD dwMode = CRYPT_MODE_ECB;
								if (TRUE == CryptSetKeyParam(hKey, KP_MODE, (BYTE*)&dwMode, 0))
								{
									if (*ptlen >= ctlen)
									{
										memcpy(plaintext, cyphertext, *ptlen);
										CryptDecrypt(hKey, NULL, isFinish, 0, plaintext, &ctlen);
										*ptlen = ctlen;
									}
									else
									{
										dwSizeNeeded = ctlen * sizeof(BYTE);
									}
								}
							}
						}
					}
				}
				CryptDestroyKey(hKey);
				CryptDestroyHash(hHash);
				CryptReleaseContext(hProv, 0);
				return dwSizeNeeded;
			}

		public:
			C3DES()
			{

			}

			~C3DES()
			{
			}



			void Error(TCHAR *str) {
				TCHAR buffer[1024];
				if (0 == FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), 0, buffer, 1024, 0)) {
					// FormatMessage failed.
				}
				_tprintf(_T("Error %s during %s!\n"), buffer, str);
			}

			void Error(int err) {
				TCHAR buffer[1024];
				if (0 == FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, err, 0, buffer, 1024, 0)) {
					// FormatMessage failed.
				}
				_tprintf(_T("%s\n"), buffer);
			}

			static String ^GetHashToString(IntPtr ^szKey, Int32 ^ KeyLen, IntPtr ^dwResult) {
				auto encrypted1 = (BYTE *)GetHash((BYTE *)szKey->ToPointer(), (int)KeyLen, (DWORD *)dwResult->ToPointer());
				DWORD * lenresD = (DWORD *)dwResult->ToPointer();
				auto encrypted = Base64Encode(encrypted1, *lenresD, (DWORD *)dwResult->ToPointer());
				String ^encryptedIntPtr = gcnew String(encrypted);
				return encryptedIntPtr;
			}

			static IntPtr ^GetHash(IntPtr ^szKey, Int32 ^ KeyLen, IntPtr ^dwResult) {
				auto encrypted = (BYTE *)GetHash((BYTE *)szKey->ToPointer(), (int)KeyLen, (DWORD *)dwResult->ToPointer());
				IntPtr ^encryptedIntPtr = gcnew IntPtr(encrypted);
				return encryptedIntPtr;
			}

			static String ^GetBase64(IntPtr ^szKey, Int32 ^ KeyLen, IntPtr ^dwResult) {
				auto encrypted = Base64Encode((BYTE *)szKey->ToPointer(), (int)KeyLen, (DWORD *)dwResult->ToPointer());
				String ^encryptedIntPtr = gcnew String(encrypted);
				return encryptedIntPtr;
			}

		
			static TCHAR *Encrypt(TCHAR *pszEncrypt, TCHAR *szKey, DWORD *dwSizeNeeded, BOOL isFinish) {
				auto encrypted = (TCHAR*)Encrypt((BYTE *)pszEncrypt,_tcslen(pszEncrypt), (BYTE *)szKey, _tcslen(szKey), dwSizeNeeded, isFinish);
				return encrypted;

			}

			static IntPtr ^Encrypt(IntPtr ^pszEncrypt, Int32 ^EncryptLen, IntPtr ^szKey, Int32 ^ KeyLen, IntPtr ^dwSizeNeeded, BOOL isFinish) {
				auto encrypted = Encrypt((BYTE *)pszEncrypt->ToPointer(), (int)EncryptLen,(BYTE *)szKey->ToPointer(), (int)KeyLen, (DWORD *)dwSizeNeeded->ToPointer(), isFinish);
				IntPtr ^encryptedIntPtr = gcnew IntPtr(encrypted);
				return encryptedIntPtr;

			}

			static String ^Encrypt(String ^pszEncrypt, String ^szKey, DWORD *dwSizeNeeded, BOOL isFinish) {
				auto _pszEncrypt = (TCHAR *)Marshal::StringToHGlobalAuto(pszEncrypt).ToPointer();
				auto _szKey = (TCHAR *)Marshal::StringToHGlobalAuto(szKey).ToPointer();
				auto encrypted = 	(TCHAR*)Encrypt((BYTE *)_pszEncrypt, pszEncrypt->Length, (BYTE *)_szKey, szKey->Length, dwSizeNeeded, isFinish);
				return gcnew String(encrypted);

			}

		


			static BYTE* Encrypt(BYTE* pszEncrypt, int EncryptLen, BYTE* szKey, int KeyLen, DWORD *dwSizeNeeded, BOOL isFinish) {
				BYTE *szUnencrypted/*[MAX_PATH * sizeof(BYTE)]*/ = new BYTE[EncryptLen + 1];
				szUnencrypted[0] = 0;
				memcpy(szUnencrypted, pszEncrypt, EncryptLen);
				//Encrypt the string with 3DES algorithm using key
				DWORD dwEncryptedSize = EncryptLen;
				*dwSizeNeeded = TripleDESencrypt(szUnencrypted, EncryptLen, szKey, KeyLen, szUnencrypted, &dwEncryptedSize, isFinish);
				if (*dwSizeNeeded != 0)
				{
					BYTE* pszEncrypted = new BYTE[*dwSizeNeeded];
					TripleDESencrypt(szUnencrypted, EncryptLen, szKey, KeyLen, pszEncrypted, &dwEncryptedSize, isFinish);
					return pszEncrypted;
				} else {
					*dwSizeNeeded = EncryptLen;
					return szUnencrypted;
				}
			}

			static String ^Decrypt(String ^pszEncrypted, String ^szKey, DWORD *dwSizeNeeded, BOOL isFinish) {
				auto _pszEncrypted = (TCHAR *)Marshal::StringToHGlobalAuto(pszEncrypted).ToPointer();
				auto _szKey = (TCHAR *)Marshal::StringToHGlobalAuto(szKey).ToPointer();
				auto encrypted = (TCHAR *)Decrypt((BYTE *)_pszEncrypted, pszEncrypted->Length,(BYTE *)_szKey, szKey->Length, dwSizeNeeded, isFinish);
				return gcnew String(encrypted);
			}

			static TCHAR* Decrypt(TCHAR *pszEncrypted, TCHAR *szKey, DWORD	*dwSizeNeeded, BOOL isFinish) {
				auto encrypted = (TCHAR*)Decrypt((BYTE *)pszEncrypted,_tcslen(pszEncrypted), (BYTE *)szKey, _tcslen(szKey), dwSizeNeeded, isFinish);
				return encrypted;
			}




			static IntPtr ^Decrypt(IntPtr ^pszEncrypt, Int32 ^EncryptLen, IntPtr ^szKey, Int32 ^ KeyLen, IntPtr ^dwSizeNeeded, BOOL isFinish) {
				auto encrypted = Decrypt((BYTE *)pszEncrypt->ToPointer(), (int)EncryptLen, (BYTE *)szKey->ToPointer(), (int)KeyLen, (DWORD*)dwSizeNeeded->ToPointer(), isFinish);
				IntPtr ^encryptedIntPtr = gcnew IntPtr(encrypted);
				return encryptedIntPtr;

			}



			static BYTE* Decrypt(BYTE* pszEncrypted, int EncryptLen, BYTE* szKey, int KeyLen, DWORD	*dwSizeNeeded, BOOL isFinish) {
				DWORD dwUnEncryptedSize = NULL;
				BYTE *szUnencrypted/*[MAX_PATH * sizeof(BYTE)]*/ = new BYTE[EncryptLen + 1]; 
				szUnencrypted[0] = 0;
				*dwSizeNeeded = TripleDESdecrypt(pszEncrypted, EncryptLen, szKey, KeyLen, szUnencrypted, &dwUnEncryptedSize, isFinish);
				BYTE	*psz3DESDecoded = new BYTE[*dwSizeNeeded];
				TripleDESdecrypt(pszEncrypted, *dwSizeNeeded, szKey, KeyLen, psz3DESDecoded, dwSizeNeeded, isFinish);
				return psz3DESDecoded;
			}
		};
	}
}