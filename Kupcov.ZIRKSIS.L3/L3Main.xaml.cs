﻿using Kupcov.Extensions;
using Kupcov.ZIRKSIS.Events;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;


namespace Kupcov.ZIRKSIS.L3
{
    public enum BlockLength
    {
        Small = 30 * 8,
        Middle = (Big + Small) /2,
        Big = 1000000
    } 

    /// <summary>
    /// Логика взаимодействия для L1Main.xaml
    /// </summary>
    public partial class L3Main : UserControl
    {


        public event EventHandler<ProgressEventArgs> CryptoProgress;

        // RSA rsa = new RSA();
        public string LabaName = "Лаба 3";
        string OpenFileNameForEncrypt { get; set; } = "";
        string SaveFileNameForEncrypt { get; set; } = "";
        string OpenFileNameForDecrypt { get; set; } = "";
        string SaveFileNameForDecrypt { get; set; } = "";
        public string KeyFileName { get; private set; }



        public ObservableCollection<Tuple<string, string>> HashList { get; set; } = new ObservableCollection<Tuple<string, string>>();
        public ObservableCollection<Tuple<string, string>> FindList { get; set; } = new ObservableCollection<Tuple<string, string>>();
        //  public event EventHandler<OpenSslVersionEventArgs> OpenSslVersion;

        DirectoryInfo
            OpenFilePathForEncrypt,
            SaveFilePathForEncrypt,
            OpenFilePathForDecrypt,
            SaveFilePathForDecrypt;

        private string latestBrowsRoot = Environment.CurrentDirectory;

        public L3Main()
        {
            InitializeComponent();
            //  OpenSslVersion?.Invoke(this, new OpenSslVersionEventArgs() {Version = OpenSSL.Core.Version.Library.ToString() });
        }

        #region Шифровка
        private void OpenFileDlgForEncrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name + ".3des";
                oShifLable.Content = OpenFileNameForEncrypt;
                sShifLable.Content = SaveFileNameForEncrypt;
            }
        }
        private void SaveEncryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.DefaultExt = ".3des";
                myDialog.Filter = "3DES shif (.3des)|*.3des";
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForEncrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private async void Encrypt_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt == null)
            {
                MessageBox.Show($"Вы не указали файл для чтения!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = false; });
            Func<string> SaveFileFunc = () =>
            {
                if (SaveFilePathForEncrypt == null)
                {
                    return OpenFilePathForEncrypt.FullName + ".3des";
                }
                else
                {
                    return SaveFilePathForEncrypt.FullName;
                }
            };
            using (FileStream file = File.Open(OpenFilePathForEncrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    if (PassName.Password == string.Empty)
                    {
                        MessageBox.Show($"Нет пароля!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                        Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
                        return;
                    }
                    byte[] b = (file.Length < (long)BlockLength.Big) ? b = new byte[(long)BlockLength.Small] : b = new byte[(long)BlockLength.Big];
                    
                   
                    byte[] bytes = PassName.Password.GetBytes();
                    IntPtr unmanagedPointerPass = Marshal.AllocHGlobal(bytes.Length);
                    Marshal.Copy(bytes, 0, unmanagedPointerPass, bytes.Length);
         
                    CryptoProgress?.Invoke(this, new ProgressEventArgs()
                    {
                        Maximum = file.Length,
                        Minimum = 0,
                        Value = 0
                    });


                    while (await file.ReadAsync(b, 0, b.Length) > 0)
                    {
                        CryptoProgress?.Invoke(this, new ProgressEventArgs()
                        {
                            Maximum = file.Length,
                            Minimum = 0,
                            Value = file.Position
                        });  
                        UTF8Encoding temp = new UTF8Encoding(true);
                        IntPtr unmanagedPointer = Marshal.AllocHGlobal(b.Length);
                        Marshal.Copy(b, 0, unmanagedPointer, b.Length);                        
                        IntPtr ii = Marshal.AllocHGlobal(b.Length);
                        var result = (IntPtr)WinCryptoApi.C3DES.Encrypt(unmanagedPointer, b.Length, unmanagedPointerPass, bytes.Length, ii,
                            (file.Position == file.Length) ? 1 : 0);
                        Marshal.FreeHGlobal(unmanagedPointer);
                        byte[] managedArray = new byte[Marshal.ReadInt32(ii)];
                        Marshal.FreeHGlobal(ii);
                        Marshal.Copy(result, managedArray, 0, managedArray.Length);
                        var res = managedArray;
                        await savefile.WriteAsync(res, 0, res.Length);
                        await savefile.FlushAsync();
                    }
                    Marshal.FreeHGlobal(unmanagedPointerPass);
                    savefile.Close();
                }
                file.Close();
                MessageBox.Show($"Файл зашифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
                Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
            }
        }
        #endregion


        #region HASH
   
        private String GetShaHase(byte[] bytePass)
        {
            IntPtr unmanagedPointerPass = Marshal.AllocHGlobal(bytePass.Length);
            Marshal.Copy(bytePass, 0, unmanagedPointerPass, bytePass.Length);
            IntPtr ii = Marshal.AllocHGlobal(bytePass.Length);
            var result = WinCryptoApi.C3DES.GetHashToString(unmanagedPointerPass, bytePass.Length, ii);
            Marshal.FreeHGlobal(unmanagedPointerPass);
            return result;
        }
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (addBox.Text.Length > 0)
            {
                var bytePass = addBox.Text.GetBytes();
                var res = GetShaHase(bytePass);
                var sel = HashList.Where(p => p.Item1.Equals(res)).Select(p => p).FirstOrDefault();
                if (sel== default(Tuple<string, string>))
                {
                    HashList.Add(new Tuple<string, string>(res, addBox.Text));

                }


            }
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {

        }

        private void findBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var sen = (TextBox)sender;
            if (sen.Text.Length > 0)
            {
                var bytePass = sen.Text.GetBytes();
                var res = GetShaHase(bytePass);
                DataGridHashTable.ItemsSource = HashList.Where(p => p.Item1.Equals(res)).Select(p => p);
            }
            else
            {
                DataGridHashTable.ItemsSource = HashList;
            }





            }
        #endregion



        #region Дешифровка
        private void SaveDecryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForDecrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForDecrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenFileDlgForDecrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            myDialog.DefaultExt = ".3des";
            myDialog.Filter = "3DES shif (.3des)|*.3des";
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForDecrypt = OpenFilePathForDecrypt.Name;
                SaveFileNameForDecrypt = Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) + '\\' +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) +
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                oShifFilePathLabel.Content = OpenFileNameForDecrypt;
                saveDeshFilePathLabel.Content = SaveFileNameForDecrypt;
            }
        }

        private async void Decrypt_Click(object sender, RoutedEventArgs e)
        {
            Func<string> SaveFileFunc = () =>
            {
                if (SaveFilePathForDecrypt == null)
                {
                    return Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) + '\\' +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) +
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                }
                else
                {
                    return SaveFilePathForDecrypt.FullName;
                }
            };
            using (FileStream file = File.Open(OpenFilePathForDecrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    if (PassName.Password == string.Empty)
                    {
                        MessageBox.Show($"Нет ключей!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    CryptoProgress?.Invoke(this, new ProgressEventArgs()
                    {
                        Maximum = file.Length,
                        Minimum = 0,
                        Value = 0
                    });
                    var bytePass = PassName.Password.GetBytes();
                    IntPtr unmanagedPointerPass = Marshal.AllocHGlobal(bytePass.Length);
                    Marshal.Copy(bytePass, 0, unmanagedPointerPass, bytePass.Length);
                    byte[] b1 = null;
                    if (file.Length < (long)BlockLength.Big)
                    {
                        b1 = new byte[(long)BlockLength.Small];
                    }
                    else
                    {
                        b1 = new byte[(long)BlockLength.Big];
                    }
                    while (await file.ReadAsync(b1, 0, b1.Length) > 0)
                    {
                        CryptoProgress?.Invoke(this, new ProgressEventArgs()
                        {
                            Maximum = file.Length,
                            Minimum = 0,
                            Value = file.Position
                        });

                        IntPtr unmanagedPointer = Marshal.AllocHGlobal(b1.Length);
                        Marshal.Copy(b1, 0, unmanagedPointer, b1.Length);
                        IntPtr ii = Marshal.AllocHGlobal(b1.Length);
                        var result = (IntPtr)WinCryptoApi.C3DES.Decrypt(unmanagedPointer, b1.Length, unmanagedPointerPass, bytePass.Length, ii, (file.Position == file.Length) ? 1 : 0);
                        Marshal.FreeHGlobal(unmanagedPointer);
                        byte[] managedArray = new byte[Marshal.ReadInt32(ii)];
                        Marshal.Copy(result, managedArray, 0, managedArray.Length);
                        await savefile.WriteAsync(managedArray, 0, managedArray.Length);
                        await savefile.FlushAsync();
                      //  Marshal.FreeHGlobal(result);
                    }
                    savefile.Close();
                    Marshal.FreeHGlobal(unmanagedPointerPass);
                }
                file.Close();
                MessageBox.Show($"Файл дешифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
