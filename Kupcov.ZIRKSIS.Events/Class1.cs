﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kupcov.ZIRKSIS.Events
{
    public enum EStatus
    {
        Work,
        Sleep,
        Stop,
        NotSet
    }
    public enum EDescription
    {
        Reboot,
        Error,
        Start,
        Stop,
        NotSet,
        Close
    }
    public class ServerStatusEventArgs : EventArgs {
        public EStatus Status { get; set; } = EStatus.NotSet;
        public EDescription Description { get; set; } = EDescription.NotSet;
    }
    public class OpenSslVersionEventArgs : EventArgs
    {
        public string Version { get; set; }
    }
    public class ProgressEventArgs : EventArgs {
        public double Maximum { get; set; }
        public double Minimum { get; set; }
        public double Value { get; set; }

    }


    public class ReciveEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
    /// <summary>
    /// EventArgs на логирование
    /// </summary>
    public class LoggerEventArgs : EventArgs
    {
        public LoggerEventArgs()
        {
            EventTime = DateTime.Now;
        }
        protected internal DateTime EventTime;
        public string Obj { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return EventTime + " > " + Obj + " " + Message;
        }
    }
}
