﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using Kupcov.Networking;

namespace Kupcov.Networking
{
    public class Net
    {
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}

namespace Kupcov.WinApi
{
    public class CWinApi
    {
        public string path;
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath">Указывает на путь к ini файлу</PARAM>
        public CWinApi(string INIPath)
        {
            if (INIPath == null)
            {
                throw new ArgumentNullException(nameof(INIPath));
            }
            path = ((Path.IsPathRooted(INIPath) == true) ? Path.GetDirectoryName(INIPath) : $"settings\\{INIPath}");
            if (Directory.Exists(((Path.IsPathRooted(INIPath) == true) ? Path.GetDirectoryName(INIPath) : $"settings")) == false)
            {
                Directory.CreateDirectory(((Path.IsPathRooted(INIPath) == true) ? Path.GetDirectoryName(INIPath) : $"settings"));
            }
            if (File.Exists(((Path.IsPathRooted(INIPath) == true) ? Path.GetDirectoryName(INIPath) : $"settings\\{INIPath}")) == false)
            {
                File.Create(((Path.IsPathRooted(INIPath) == true) ? Path.GetDirectoryName(INIPath) : $"settings\\{INIPath}"));
            }
        }

        /// <summary>
        /// Запись данных в INI файл
        /// </summary>
        /// <PARAM name="Section">Section Name</PARAM> 
        /// <PARAM name="Key">Key Name</PARAM>
        /// <PARAM name="Value">Value Name</PARAM>     
        public void INIWriteValue(string Section, string Key, string Value)
        {
            if (Section == null)
            {
                throw new ArgumentNullException(nameof(Section));
            }
            if (Key == null)
            {
                throw new ArgumentNullException(nameof(Key));
            }
            if (Value == null)
            {
                throw new ArgumentNullException(nameof(Value));
            }
            WritePrivateProfileString(Section, Key, Value, path);
        }

        /// <summary>
        /// Чтение данных из INI файла
        /// </summary>
        /// <param name="Section">Секция</PARAM>
        /// <param name="Key">Ключ</PARAM>
        /// <returns></returns>
        public string INIReadValue(string Section, string Key)
        {
            if (Section == null)
            {
                throw new ArgumentNullException(nameof(Section));
            }
            if (Key == null)
            {
                throw new ArgumentNullException(nameof(Key));
            }
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            if (i != 0)
            {
                if (Section.CompareTo("Global_settings") == 0)
                {
                    var str = temp.ToString();
                    Uri uri = new Uri(str);
                    string hostIP = Net.GetLocalIPAddress();
                    if (uri.Host.CompareTo(hostIP) == 0)
                    {
                        return uri.LocalPath;
                    }
                    else
                    {
                        Uri baseURI = new Uri("\\\\" + hostIP);
                        Uri newURI = new Uri(baseURI, uri.AbsolutePath.Substring(1));
                        INIWriteValue("Global_settings", "template_html", newURI.ToString());
                        return newURI.LocalPath;
                    }
                }
            }
            return temp.ToString();
        }
    }
}
