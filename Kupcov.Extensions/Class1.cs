﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kupcov.Extensions
{
    public static class MyExtencions
    {
        public static byte[] GetBytes(this string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
        public static string GetString(this byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }

        public static char[] ToCharArray(this byte[] bytes)
        {
            char[] arr = new char[bytes.Length];
            for (int i = 0; i < bytes.Length; i++)
            {
                arr[i] = (char)bytes[i];
            }
            return arr;
        }


        public static async Task SenderAsync(this Stream sslStream, string mes)
        {
            var byteMes = mes.GetBytes();
            var das = BitConverter.GetBytes(byteMes.Length);
            await sslStream.WriteAsync(das, 0, das.Length);
            await sslStream.WriteAsync(byteMes, 0, byteMes.Length);
        }

        public static async Task<string> ReciverAsync(this Stream sslStream)
        {
            var das = new byte[256];
            await sslStream.ReadAsync(das, 0, das.Length);
            int reciveMessageLength = BitConverter.ToInt32(das, 0);
            das = new byte[reciveMessageLength];
            await sslStream.ReadAsync(das, 0, das.Length);
            return das.GetString();
        }


    }
}
