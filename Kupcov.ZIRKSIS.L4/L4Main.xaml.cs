﻿using Kupcov.Extensions;
using Kupcov.ZIRKSIS.Events;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;


namespace Kupcov.ZIRKSIS.L4
{
    public enum BlockLength
    {
        Small = 30 * 16,
        Middle = (Big + Small) / 2,
        Big = 1000000
    }

    /// <summary>
    /// Логика взаимодействия для L1Main.xaml
    /// </summary>
    public partial class L4Main : UserControl
    {
        public event EventHandler<ProgressEventArgs> CryptoProgress;

        // RSA rsa = new RSA();
        public string LabaName = "Лаба 4";
        string OpenFileNameForEncrypt { get; set; } = "";
        string SaveFileNameForEncrypt { get; set; } = "";
        string OpenFileNameForDecrypt { get; set; } = "";
        string SaveFileNameForDecrypt { get; set; } = "";

        DirectoryInfo
            OpenFilePathForEncrypt,
            SaveFilePathForEncrypt,
            OpenFilePathForDecrypt,
            SaveFilePathForDecrypt;

        private string latestBrowsRoot = Environment.CurrentDirectory;

        public L4Main()
        {
            InitializeComponent();
            //  OpenSslVersion?.Invoke(this, new OpenSslVersionEventArgs() {Version = OpenSSL.Core.Version.Library.ToString() });
        }

        #region Шифровка
        private void OpenFileDlgForEncrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name + ".aes";
                oShifLable.Content = OpenFileNameForEncrypt;
                sShifLable.Content = SaveFileNameForEncrypt;
            }
        }
        private void SaveEncryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.DefaultExt = ".aes";
                myDialog.Filter = "AES shif (.aes)|*.aes";
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForEncrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private async void Encrypt_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt == null)
            {
                MessageBox.Show($"Вы не указали файл для чтения!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = false; });
            Func<string> SaveFileFunc = () =>
            {
                if (SaveFilePathForEncrypt == null)
                {
                    return OpenFilePathForEncrypt.FullName + ".aes";
                }
                else
                {
                    return SaveFilePathForEncrypt.FullName;
                }
            };
            if (PassName.Password == string.Empty)
            {
                MessageBox.Show($"Нет пароля!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
                return;
            }



            using (FileStream file = File.Open(OpenFilePathForEncrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    AesCrypto aesCrypto = new AesCrypto(AesMode.Encrypt, PassName.Password, file, savefile);
                    aesCrypto.CryptoProgress += CryptoProgress;
                    await aesCrypto.EncryptStreamAsync();
                    savefile.Close();

                }
                MessageBox.Show($"Файл зашифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
                Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
                file.Close();
            }

        }
        #endregion





        #region Дешифровка
        private void SaveDecryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForDecrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForDecrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenFileDlgForDecrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            myDialog.DefaultExt = ".aes";
            myDialog.Filter = "AES shif (.aes)|*.aes";
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForDecrypt = OpenFilePathForDecrypt.Name;
                SaveFileNameForDecrypt = Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) + '\\' +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) +
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                oShifFilePathLabel.Content = OpenFileNameForDecrypt;
                saveDeshFilePathLabel.Content = SaveFileNameForDecrypt;
            }
        }

        private async void Decrypt_Click(object sender, RoutedEventArgs e)
        {
            Func<string> SaveFileFunc = () =>
            {
                if (SaveFilePathForDecrypt == null)
                {
                    return Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) + '\\' +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) +
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                }
                else
                {
                    return SaveFilePathForDecrypt.FullName;
                }
            };
            if (PassName.Password == string.Empty)
            {
                MessageBox.Show($"Нет ключей!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            using (FileStream file = File.Open(OpenFilePathForDecrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    AesCrypto aesCrypto = new AesCrypto(AesMode.Decrypt, PassName.Password, file, savefile);
                    aesCrypto.CryptoProgress += CryptoProgress;
                    await aesCrypto.DecryptStreamAsync();
                    savefile.Close();
                }
                file.Close();
                MessageBox.Show($"Файл дешифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
