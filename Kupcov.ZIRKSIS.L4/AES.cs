﻿using Kupcov.Extensions;
using Kupcov.ZIRKSIS.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Kupcov.ZIRKSIS.L4
{
    public enum AesMode {
        Encrypt,
        Decrypt
    }
    public class AesCrypto
    {
        public event EventHandler<ProgressEventArgs> CryptoProgress;
        AesManaged aesAlg;
        ICryptoTransform encryptor;
        ICryptoTransform decryptor;
        FileStream saveFileStream;
        FileStream readFileStream;
        CryptoStream csEncrypt;
        StreamWriter swEncrypt;
        StreamReader srDecrypt;
        CryptoStream csDecrypt;

        public AesCrypto(AesMode aesMode, string password, FileStream readFS, FileStream saveFS)
        {
            aesAlg = new AesManaged();
            aesAlg.Key = PasswordAsByte(password);
            aesAlg.IV = aesAlg.Key.Take(16).ToArray();
            readFileStream = readFS;
            saveFileStream = saveFS;
            if (aesMode == AesMode.Encrypt)
            {
                encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                csEncrypt = new CryptoStream(saveFileStream, encryptor, CryptoStreamMode.Write);
                swEncrypt = new StreamWriter(csEncrypt);
            }
            else
            {
                decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                csDecrypt = new CryptoStream(readFileStream, decryptor, CryptoStreamMode.Read);
                srDecrypt = new StreamReader(csDecrypt);

            }
        }
        private static byte[] PasswordAsByte(string password)
        {
            byte[] salt = Encoding.Default.GetBytes("foobar42");
            Rfc2898DeriveBytes passwordBytes = new Rfc2898DeriveBytes(password, salt);
            return passwordBytes.GetBytes(32);
        }


        public async Task EncryptStreamAsync()
        {
            CryptoProgress?.Invoke(this, new ProgressEventArgs()
            {
                Maximum = readFileStream.Length,
                Minimum = 0,
                Value = 0
            });
            int data;
            byte[] b = (readFileStream.Length < (long)BlockLength.Big) ? b = new byte[(long)BlockLength.Small] : b = new byte[(long)BlockLength.Big];
            while ((data = await readFileStream.ReadAsync(b, 0, b.Length)) > 0)
            {
                CryptoProgress?.Invoke(this, new ProgressEventArgs()
                {
                    Maximum = readFileStream.Length,
                    Minimum = 0,
                    Value = readFileStream.Position
                });
                await csEncrypt.WriteAsync(b, 0, data);            
            }

            csEncrypt.Close();
        }




        public async Task DecryptStreamAsync()
        {
            CryptoProgress?.Invoke(this, new ProgressEventArgs()
            {
                Maximum = readFileStream.Length,
                Minimum = 0,
                Value = 0
            });
            int data;
            byte[] b = (readFileStream.Length < (long)BlockLength.Big) ? b = new byte[(long)BlockLength.Small] : b = new byte[(long)BlockLength.Big];
            while ((data = await csDecrypt.ReadAsync(b, 0, b.Length)) > 0)

            {
                CryptoProgress?.Invoke(this, new ProgressEventArgs()
                {
                    Maximum = readFileStream.Length,
                    Minimum = 0,
                    Value = readFileStream.Position
                });
                await saveFileStream.WriteAsync(b, 0, data);
            }
            
            csDecrypt.Close();
        }
    }
   
}
