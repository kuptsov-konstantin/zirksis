﻿using System;
using System.Runtime.Serialization;

namespace Kupcov.ZIRKSIS.L2
{
    [Serializable]
    internal class PortUnavailableException : Exception
    {
        public PortUnavailableException()
        {
        }

        public PortUnavailableException(string message) : base(message)
        {
        }

        public PortUnavailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PortUnavailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}