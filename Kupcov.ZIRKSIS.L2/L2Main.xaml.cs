﻿using Kupcov.ZIRKSIS.Events;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Kupcov.ZIRKSIS.L2
{
    public class GroupSearchConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? _val = value as double?;
            if (_val == null)
            {
                throw new ArgumentException($"Не правильное поле {nameof(value)}");
            }
            double? _par = parameter as double?;
            if (_val == null)
            {
                throw new ArgumentException($"Не правильное поле {nameof(parameter)}");
            }
            return _val - _par - 5;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public enum WindowsType
    {
        Server,
        Client
    }

    public class WindowInfo: INotifyPropertyChanged
    {
        public WindowInfo()
        {

        }
        public WindowInfo(object obj)
        {
            Window = obj;
            (Window as ServerWindow).Server.StatusChanged += (o, e) => {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Status"));
            };
        }
        public int Id { get; set; }
        public WindowsType ThisType { get; set; }
        public int Port { get; set; }

        public string Name { get; set; }
        public string Status
        {
            get { return (Window as ServerWindow).Server.Status.ToString(); }
        }

        public object Window { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class L2Main : UserControl, INotifyPropertyChanged
    {

        public event EventHandler<OpenSslVersionEventArgs> OpenSslVersion;
        public string LabaName = "Лаба 2";
        //  List<object> _allWindow = new List<object>();
        ObservableCollection<WindowInfo> _allWindowInfo = new ObservableCollection<WindowInfo>();

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<WindowInfo> AllWindow { get { return _allWindowInfo; } set { _allWindowInfo = value; } }
        public L2Main()
        {
            InitializeComponent();
          
        }

        private void StartMainServerButton_Click(object sender, RoutedEventArgs e)
        {
            ServerWindow win = new ServerWindow("Главный сервер", Ms_Logger);
            win.Show();
            win.Closed += Win_Closed;
            win.Start();
            win.ServerAdded += (o, e1) =>
            {
                AllWindow.Add(new WindowInfo(e1) { Id = ((AllWindow.Count() == 0) ? 0 : AllWindow.Last().Id + 1), ThisType = WindowsType.Server, Name = e1.ServerName, Port = e1.Port });
            };
            win.ServerAddedClosed += Win_Closed;
            AllWindow.Add(new WindowInfo(win) { Id = ((AllWindow.Count() == 0) ? 0 : AllWindow.Last().Id + 1), ThisType = WindowsType.Server, Name = win.ServerName, Port = win.Port });
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerName"));
        }

        private void Win_Closed(object sender, EventArgs e)
        {
            var res = AllWindow.Select(p => p).Where(p => p.Window == sender).FirstOrDefault();
            //  _allWindow.Remove(res);
            var resRem = AllWindow.Remove(res);
        }

        private void Ms_Logger(object sender, LoggerEventArgs e)
        {
            try
            {
                listBox.Dispatcher.Invoke(() => listBox.Items.Add(e.ToString()));
            }
            catch (Exception)
            {

            }
        }

        private async void Search_Click(object sender, RoutedEventArgs e)
        {
            string str = SerchTextBox.Text;
            MyClient mc = new MyClient("Client");
            mc.Logger += Ms_Logger;
            mc.Recive += Mc_Recive;

            await mc.Start();
            mc.ConnectAndSend("localhost", 777, str);
        }

        private void Mc_Recive(object sender, ReciveEventArgs e)
        {
            string[] res = e.Message.Split('\n');
            foreach (var item in res)
            {
                Ms_Logger(this, new LoggerEventArgs() { Obj = "Client.Recive", Message = item });
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var HeightD = e.NewSize.Height - ((e.PreviousSize.Height == 0) ? e.NewSize.Height : e.PreviousSize.Height);
            var WidthD = e.NewSize.Width - ((e.PreviousSize.Width == 0) ? e.NewSize.Width : e.PreviousSize.Width);
        }

        private void ShowHideDetailsClick(object sender, RoutedEventArgs e)
        {
            var info = ((sender as Button).DataContext as WindowInfo);
            if (info != null)
            {
                var win = info.Window as Window;
                win.Show();
            }
        }

        private void l2mainwindow_Loaded(object sender, RoutedEventArgs e)
        {
            OpenSslVersion?.Invoke(this, new OpenSslVersionEventArgs() { Version = OpenSSL.Core.Version.Library.ToString() });
            Ms_Logger(this, new LoggerEventArgs() { Obj = "OpenSSL", Message = MyClient.OpensslVersion.ToString() });
            if(Directory.Exists("save") == false)
            {
                Directory.CreateDirectory("save");
            }
            var files_save = Directory.GetFiles("save");
            foreach (var item in files_save)
            {
                File.Delete(item);
            }


            // listBox.Dispatcher.Invoke(() => listBox.Items.Add(MyClient.OpensslVersion));
        }

        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            string collectedText = "";
            foreach (string dd in listBox.SelectedItems)
            {
                collectedText += dd + Environment.NewLine;
            }
            if (listBox.SelectedItems != null)
            {
                Clipboard.SetText(collectedText);
            }
        }

        public async Task CloseServers(int port)
        {
            /*  foreach (var item in AllWindow)
              {*/
            var main = _allWindowInfo.Where(p => p.Port == port).Select(p => p).FirstOrDefault();
            if (main != default(WindowInfo))
            {
                var server = main.Window as ServerWindow;
                await server.Stop(EDescription.Close);
                server.Close();
            }

            /*  }*/
        }

        private async void ShotdownServerClick(object sender, RoutedEventArgs e)
        {
            var buttonContext = ((sender as Button).DataContext as WindowInfo); 
             await CloseServers(buttonContext.Port);
        }
    }
}