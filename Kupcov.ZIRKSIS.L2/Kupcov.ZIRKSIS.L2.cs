﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.ObjectModel;
using System.IO;
using OpenSSL.X509;
using OpenSSL.SSL;
using OpenSSL.Crypto;
using OpenSSL.Core;
using Kupcov.ZIRKSIS.Events;
using System.Net.NetworkInformation;
using Kupcov.WinApi;
using Kupcov.Extensions;

namespace Kupcov.ZIRKSIS.L2
{
    public class SslTestContext : IDisposable
    {
        public SslTestContext()
        {
            CWinApi settingFile = new CWinApi("settings.ini");
            using (var cfg = new Configuration(settingFile.INIReadValue("crypto", "cryptofile")))
            using (var ca = X509CertificateAuthority.SelfSigned(cfg, new SimpleSerialNumber(), "Root", DateTime.Now, TimeSpan.FromDays(365)))
            {
                CAChain.Add(ca.Certificate);
                ServerCertificate = CreateCertificate(ca, "server", cfg, "tls_server");
                ClientCertificate = CreateCertificate(ca, "client", cfg, "tls_client");
            }
            ClientCertificateList.Add(ClientCertificate);
        }

        public SslTestContext(string path)
        {
            using (var cfg = new Configuration(path))
            using (var ca = X509CertificateAuthority.SelfSigned(
                                cfg,
                                new SimpleSerialNumber(),
                                "Root",
                                DateTime.Now,
                                TimeSpan.FromDays(365)))
            {
                CAChain.Add(ca.Certificate);

                ServerCertificate = CreateCertificate(ca, "server", cfg, "tls_server");
                ClientCertificate = CreateCertificate(ca, "client", cfg, "tls_client");
            }

            ClientCertificateList.Add(ClientCertificate);
        }
        X509Certificate CreateCertificate(X509CertificateAuthority ca, string name, Configuration cfg, string section)
        {
            var now = DateTime.Now;
            var future = now + TimeSpan.FromDays(365);

            using (var subject = new X509Name(name))
            using (var rsa = new RSA())
            {
                rsa.GenerateKeys(1024, BigNumber.One, null, null);
                using (var key = new CryptoKey(rsa))
                {
                    var request = new X509Request(1, subject, key);
                    var cert = ca.ProcessRequest(request, now, future, cfg, section);
                    cert.PrivateKey = key;
                    return cert;
                }
            }
        }

        public X509Chain CAChain = new X509Chain();
        public X509List ClientCertificateList = new X509List();
        public X509Certificate ServerCertificate;
        public X509Certificate ClientCertificate;

        #region IDisposable implementation

        public void Dispose()
        {
            ClientCertificateList.Clear();
            CAChain.Dispose();
            ServerCertificate.Dispose();
            ClientCertificate.Dispose();
        }

        #endregion
    }
    
    public interface IMyLogger
    {
        event EventHandler<LoggerEventArgs> Logger;
    }


    public class MyClient : IMyLogger
    {
        public event EventHandler<LoggerEventArgs> Logger;
        public event EventHandler<ReciveEventArgs> Recive;

        public TcpClient tcpClient;
        SslStream sslStream = null;

        public string clientName = "";

        public MyClient(string _name)
        {
            clientName = _name;
        }

        public async Task Start()
        {
            await Task.Factory.StartNew(
                () =>
                {

                    try
                    {
                        tcpClient = new TcpClient(AddressFamily.InterNetwork);
                        Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Init" });

                    }
                    catch (Exception e)
                    {
                        Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"{e.Message.ToString()}" });
                        Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Close" });
                        tcpClient.Close();
                    }

                });
        }

        public void ConnectAndSend(string hostname, int port, string mes)
        {
            try
            {
                var evtDone = new AutoResetEvent(false);
                tcpClient.BeginConnect(hostname, port, (IAsyncResult ar) =>
                {
                    tcpClient.EndConnect(ar);

                    var sslStream = new SslStream(tcpClient.GetStream());
                    Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Authenticate" });
                    sslStream.BeginAuthenticateAsClient(hostname, async (ar2) =>
                    {
                        try
                        {
                            sslStream.EndAuthenticateAsClient(ar2);
                            Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"CurrentCipher: {sslStream.Ssl.CurrentCipher.Name}" });
                            await sslStream.SenderAsync(mes);
                            string result = await sslStream.ReciverAsync();
                            sslStream.Close();
                            tcpClient.Close();
                            Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"done" });
                            evtDone.Set();
                            Recive?.Invoke(this, new ReciveEventArgs() { Message = result });
                        }
                        catch (Exception e)
                        {
                            Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"{e.Message.ToString()}" });
                            Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Close" });
                        }

                    }, null);
                }, null);
            }
            catch (Exception e)
            {
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"{e.Message.ToString()}" });
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Close" });
            }
        }


        public async Task<string> Send(string mes)
        {
            if (sslStream == null)
            {
                throw new ArgumentNullException("Вы не запустили сервер!");
            }
            var serverTask = await Task.Factory.StartNew(async () =>
            {
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Send: {mes}" });
                var mesbytes = mes.GetBytes();
                await sslStream.WriteAsync(mesbytes, 0, mes.GetBytes().Length);
                byte[] data = new byte[256];
                string responseData = string.Empty;
                int bytes = await sslStream.ReadAsync(data, 0, data.Length);
                responseData = Encoding.ASCII.GetString(data, 0, bytes);
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = clientName, Message = $"Received: {responseData}" });
                return responseData;
            });
            return null;

        }

        public static OpenSSL.Core.Version OpensslVersion
        {
            get
            {
                return OpenSSL.Core.Version.Library;
            }
        }
    }

    public static class HttpStatusText
    {

        public static string NotFound { get { return "404 - Not found :(( "; } }
    }

    public class MainServer : IMyLogger
    {
        SslTestContext _ctx = new SslTestContext();
        public static OpenSSL.Core.Version OpensslVersion
        {
            get
            {
                return OpenSSL.Core.Version.Library;
            }
        }

        /// <summary>
        /// Ивент на ведение лога
        /// </summary>
        public event EventHandler<LoggerEventArgs> Logger;
        public event EventHandler<ServerStatusEventArgs> StatusChanged;
        public TcpListener tcpListener;

        private EStatus _status = EStatus.NotSet;
        private EDescription _description = EDescription.NotSet;
        public EStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                StatusChanged?.Invoke(this, new ServerStatusEventArgs() { Status = value, Description = _description });
            }
        }
        public ObservableCollection<string> files = null;
        public MainServer(string name)
        {
            serverName = name;
        }
        public static ManualResetEvent tcpClientConnected = new ManualResetEvent(false);

        public Task ServerTask = null;

        bool workingstatus = true;

        /// <exception cref="PortUnavailableException">Why it's thrown.</exception>
        public MainServer StartThread(int port, Action<Task> action)
        {
            ServerTask = new Task(
                    () =>
                    {
                        Start(port);

                    });
            ServerTask.ContinueWith(action, TaskContinuationOptions.OnlyOnFaulted);
            ServerTask.Start();
            return this;
        }
        

        private string serverName;

        public string FileWithChildrens { get; set; }

        public static int GetFreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }

        bool testPORT(int port)
        {
            bool isAvailable = true;
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();
            foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == port)
                {
                    isAvailable = false;
                    break;
                }
            }
            return isAvailable;
        }

        private async Task<string> FindInOtherServersAsync(string findmes)
        {
            if (File.Exists(FileWithChildrens))
            {
                var res = File.ReadAllLines(FileWithChildrens);
                string global_res = "";
                ManualResetEvent clientsFlag = new ManualResetEvent(false);
                foreach (var item in res)
                {
                    Uri url;
                    IPAddress ip;
                    if (Uri.TryCreate(string.Format($"http://{item}"), UriKind.Absolute, out url) && IPAddress.TryParse(url.Host, out ip))
                    {
                        clientsFlag.Reset();
                        IPEndPoint endPoint = new IPEndPoint(ip, url.Port);
                        MyClient mc = new MyClient("Client." + serverName);
                        mc.Logger += Logger;
                        await mc.Start();
                        mc.Recive += (e, k) =>
                        {
                            global_res += k.Message;
                            clientsFlag.Set();
                        };

                        mc.ConnectAndSend(endPoint.Address.ToString(), endPoint.Port, findmes);
                        clientsFlag.WaitOne();
                    }
                }
                return global_res;
            }
            else
            {
                return string.Empty;
            }
        }

        public MainServer Start(int port)
        {
            if (testPORT(port) == false)
            {
                throw new PortUnavailableException("Порт занят");
            }

            try
            {
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = "Запуск главного сервера" });
                IPAddress ipAddress = Dns.GetHostEntry("localhost").AddressList[1];
                tcpListener = new TcpListener(ipAddress, port);
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"{ipAddress.ToString()}. Порт: {port}" });
                tcpListener.Start();
                Status = EStatus.Work;
                while (true == workingstatus)
                {
                    tcpClientConnected.Reset();
                    Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"Waiting for a connection..." });
                    tcpListener.BeginAcceptTcpClient(
                        (IAsyncResult ar) =>
                        {
                            try
                            {
                                tcpClientConnected.Set();
                                var client = tcpListener.EndAcceptTcpClient(ar);
                                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"New connection {client.Client.LocalEndPoint.ToString()}" });


                                var sslStream = new SslStream(client.GetStream(), false);
                                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"authenticate {client.Client.LocalEndPoint.ToString()}" });
                                sslStream.BeginAuthenticateAsServer(_ctx.ServerCertificate, async (ar2) =>
                                {
                                    sslStream.EndAuthenticateAsServer(ar2);
                                    Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"CurrentCipher {sslStream.Ssl.CurrentCipher.Name}" });
                                    string findMess = await sslStream.ReciverAsync();
                                    string result = string.Empty;
                                    var ee = await FindInOtherServersAsync(findMess);
                                    if (files != null)
                                    {
                                        result = string.Join("\n", files.Select(p => p).Where(p => p.ToLower().Contains(findMess.ToLower())).ToArray());
                                    }
                                    else
                                    {
                                        result = HttpStatusText.NotFound;
                                    }

                                    if (result.Length == 0)
                                    {
                                        result = HttpStatusText.NotFound;
                                    }
                                    await sslStream.SenderAsync(result + Environment.NewLine + ee);

                                    Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"Done {client.Client.LocalEndPoint.ToString()}" });
                                    client.Close();
                                }, null);
                            }
                            catch (Exception e)
                            {
                                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"{e.Message.ToString()}" });
                            }
                        },
                        tcpListener);
                    tcpClientConnected.WaitOne();
                }
            }
            catch (Exception e)
            {
                Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"{e.Message.ToString()}" });
                tcpListener?.Stop();
                Status = EStatus.Stop;
            }
            return this;
        }

        public async Task<bool> Stop(EDescription description)
        {
            _description = description;
            return await Task.Factory.StartNew(() =>
             {
                 try
                 {

                     Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"Stop: Сервер пробует остановится" });

                     if (ServerTask != null)
                     {
                         workingstatus = false;
                         tcpListener.Stop();
                         ServerTask.Dispose();
                     }
                     else
                     {
                         workingstatus = false;
                         tcpListener.Stop();
                     }
                     Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"Stop: Сервер остановлен" });
                     Status = EStatus.Stop;
                     return true;
                 }
                 catch (Exception e)
                 {
                     Logger?.Invoke(this, new LoggerEventArgs() { Obj = serverName, Message = $"{e.Message.ToString()}" });
                     return false;
                 }
             }
             );
        }
    }
}
