﻿using Kupcov.ZIRKSIS.Events;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Kupcov.ZIRKSIS.L2
{
    public class ServerNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class ServerWindow : Window, INotifyPropertyChanged, IDisposable
    {
        private string _name = "";
        private DateTime _startServerTime;
        private TimeSpan _workingServerTime;
        public event EventHandler<LoggerEventArgs> Logger;        
        private Action<object, LoggerEventArgs> ms_Logger;
        public event EventHandler<ServerWindow> ServerAdded;
        public event EventHandler ServerAddedClosed;
        private string _path;

        public int Port { get; set; }

        public ObservableCollection<ServerWindow> AdditionlServers { get; set; } = new ObservableCollection<ServerWindow>();

        public string MySaveFile { get { return $"save\\{ServerName}.additionalserver.txt"; } }



        private ServerWindow _parentWindow;
        public ServerWindow ParentServer
        {
            get { return _parentWindow; }
            set
            {
                _parentWindow = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ParentServer"));
            }
        }



        public ObservableCollection<string> Files { get; set; } = new ObservableCollection<string>();

        MainServer ms = null;
        public event PropertyChangedEventHandler PropertyChanged;

        System.Timers.Timer timer;

        public MainServer Server { get { return ms; } }

        public ServerWindow()
        {
            InitializeComponent();
        }

        public ServerWindow(string name) : this()
        {
            ms = new MainServer(name);
            ServerName = name;
        }

        public ServerWindow(string name, Action<object, LoggerEventArgs> ms_Logger, ServerWindow _parentServer = null) : this(name)
        {
            AdditionlServers.CollectionChanged += AdditionlServers_CollectionChanged;
            

            ParentServer = ParentServer;
            this.ms_Logger = ms_Logger;
            ms.Logger += (o, ea) => ms_Logger(o, ea); ;
            ms.files = Files;
            ms.StatusChanged += Ms_StatusChanged;
            var autoEvent = new AutoResetEvent(false);
            timer = new System.Timers.Timer((new TimeSpan(0, 0, 1)).Ticks / 10000);
            timer.Elapsed += (sender, e) =>
            {
                ServerWorkingTime = DateTime.Now - _startServerTime;
            };
        }

        private void AdditionlServers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
            }
            if (e.NewItems != null)
            {
                Directory.CreateDirectory("save");
                if (!File.Exists(MySaveFile))
                {
                    FileStream fstream = File.Create(MySaveFile);
                    fstream.Close();
                }
                foreach (var item in e.NewItems)
                {
                    var win = item as ServerWindow;
                    if (win.ParentServer == this)
                    {
                        win.Server.StatusChanged += (s, e1) =>
                        {
                            if (e1.Status == EStatus.Work)
                            {
                                var mains = s as MainServer;
                                File.AppendAllText(MySaveFile, $"{mains.tcpListener.LocalEndpoint.ToString()}" + Environment.NewLine);
                            }
                        };
                    }
                }
            }
        }

        private void Ms_StatusChanged(object sender, ServerStatusEventArgs e)
        {
            if (e.Description != EDescription.Reboot)
            {
                if (e.Status == EStatus.Stop)
                {
                    this?.Dispatcher.Invoke(() => Close());
                }
            }
        }

        public async Task<MainServer> Start()
        {
            return await Start(777); //<< Главный сервер всегда на 777
        }
        public async Task< MainServer> Start(int port)
        {
            timer.Start();
            Port = port;
            ServerStartTime = DateTime.Now;
            ms.FileWithChildrens = MySaveFile;
            return ms.StartThread(port, async (e) =>
            {
                try
                {
                    var exception = e.Exception;
                }
                catch (PortUnavailableException)
                {
                    timer.Stop();
                    await ms.Stop(EDescription.Error);
                }
            });
        }

        public async Task Stop(EDescription description = EDescription.NotSet)
        {
            ManualResetEvent closingServer = new ManualResetEvent(false);
            var serv = new ObservableCollection<ServerWindow>(AdditionlServers.Where(p => p.ParentServer == this).ToList());
            foreach (ServerWindow item in serv)
            {
                closingServer.Reset();

                item.Server.StatusChanged += (o, e) =>
                {
                    if (e.Status == EStatus.Stop)
                    {                        
                        closingServer.Set();
                    }
                };

                await item.Stop(description);
                closingServer.WaitOne();

            }
            timer.Stop();
        }

        public string ServerName
        {
            get
            {
                return $"{_name}";
            }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerName"));
            }
        }

        public DateTime ServerStartTime
        {
            get
            {
                return _startServerTime;
            }
            set
            {
                _startServerTime = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerStartTime"));
            }
        }
        public TimeSpan ServerWorkingTime
        {
            get
            {
                return _workingServerTime;
            }
            set
            {
                _workingServerTime = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerWorkingTime"));
            }
        }
        public string ServerStatus
        {
            get
            {
                return $"{_name}";
            }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerStatus"));
            }
        }
        public string ServerPath
        {
            get
            {
                return $"{_path}";
            }
            set
            {
                _path = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerPath"));
            }
        }
        public string ServerFoundFilesCount
        {
            get
            {
                return $"{Files.Count}";
            }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerFoundFilesCount"));
            }
        }
        public string ServerStatusUpdate
        {
            get
            {
                return $"{_name}";
            }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerStatusUpdate"));
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ParentServer != null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ParentServer"));
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ServerName"));
        }

        private async void start_button_Click(object sender, RoutedEventArgs e)
        {
          await  Start(Port);
        }

        private async void stop_button_Click(object sender, RoutedEventArgs e)
        {
            await Stop();
        }

        private void update_button_Click(object sender, RoutedEventArgs e)
        {
            Files.Clear();
            UpdateFiles();
        }

        private void UpdateFiles(DirectoryInfo di = null)
        {
            if(_path == null)
            {
                return;
            }

            if (di == null)
            {
                di = new DirectoryInfo(_path);
            }


            var folders = di.GetDirectories();
            foreach (var directory in folders)
            {
                UpdateFiles(directory);
            }
            var files = di.GetFiles();
            foreach (var file in files)
            {
                Files.Add(file.FullName);
            }
        }


        private void ofd_button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ServerPath = folderBrowserDialog1.SelectedPath;
            }
        }

        private async void restart_button_Click(object sender, RoutedEventArgs e)
        {
            await Stop(EDescription.Reboot);
            await Start(Port);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (ms.Status == EStatus.Work)
            {
                e.Cancel = true;
                this.Visibility = Visibility.Hidden;
            }

            if (ms.Status == EStatus.Stop)
            {
                foreach (var item in AdditionlServers)
                {
                    if (item.ParentServer == this)
                    {
                        item.Close();
                    }
                }
                e.Cancel = false;
            }
        }


        public ServerWindow StartAdditionalServer()
        {
            ServerWindow win = new ServerWindow($"{this?.ServerName}.Д{AdditionlServers.Count}", this.ms_Logger, this);
            win.ParentServer = this;
            win.Show();
            win.Closed += Win_Closed;
            win.ServerAdded += (o, e1) =>
            {
                AdditionlServers.Add(e1);
                ServerAdded?.Invoke(o, e1);
            };


            int freePort = MainServer.GetFreeTcpPort();
            win.Start(freePort);



            AdditionlServers.Add(win);
            ServerAdded?.Invoke(this, win);
            return win;
        }
        private void AddAdditionalServerButton_Click(object sender, RoutedEventArgs e)
        {
            StartAdditionalServer();
        }

        private void Win_Closed(object sender, EventArgs e)
        {
   
            var res = AdditionlServers.Select(p => p).Where(p => p == sender).FirstOrDefault();
            var resRem = AdditionlServers.Remove(res);
            ServerAddedClosed?.Invoke(sender, e);
            // throw new NotImplementedException();
        }

        public void Dispose()
        {
          
        }
    }
}