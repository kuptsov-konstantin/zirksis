﻿using System.Windows;
using Kupcov.WinApi;
using Microsoft.Win32;

namespace ZirksisKupcovAgregator
{
    /// <summary>
    /// Логика взаимодействия для SettingWindow.xaml
    /// </summary>
    public partial class SettingWindow : Window
    {
        private CWinApi settingFile;
        
        public SettingWindow()
        {
            InitializeComponent();
        }

        public SettingWindow(string _settingFile) : this()
        {
            this.settingFile = new CWinApi( _settingFile);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Filter = "cnf файлы (*.cnf)|*.cnf|Все файлы (*.*)|*.*";
            myDialog.Multiselect = false;
            if (myDialog.ShowDialog() == true)
            {
                textBox.Dispatcher.Invoke(() => textBox.Text = myDialog.FileName);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            textBox.Dispatcher.Invoke(() => textBox.Text = this.settingFile.INIReadValue("crypto","cryptofile"));
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            this.settingFile.INIWriteValue("crypto", "cryptofile", textBox.Text);
        }

        private void buttonExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
