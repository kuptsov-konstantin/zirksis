﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZirksisKupcovAgregator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuAbout_Click(object sender, RoutedEventArgs e)
        {
            new About().Show();
        }

        private void MenuHelp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TabItem1.Header = Laba1.LabaName;
            TabItem2.Header = Laba2.LabaName;
            TabItem3.Header = Laba3.LabaName;
            TabItem4.Header = Laba4.LabaName;
            Laba1.OpenSslVersion += Laba_OpenSslVersion;
            Laba2.OpenSslVersion += Laba_OpenSslVersion;
            Laba3.CryptoProgress += Laba3_CryptoProgress;
            Laba4.CryptoProgress += Laba3_CryptoProgress;
        }

        private void Laba3_CryptoProgress(object sender, Kupcov.ZIRKSIS.Events.ProgressEventArgs e)
        {
            ProgressBarName.Dispatcher.Invoke(()=> {
                ProgressBarName.Maximum = e.Maximum;
                ProgressBarName.Minimum = e.Minimum;
                ProgressBarName.Value = e.Value;
            });
        }

        private void Laba_OpenSslVersion(object sender, Kupcov.ZIRKSIS.Events.OpenSslVersionEventArgs e)
        {
            StatusOpenSSl.Dispatcher.Invoke(() => { StatusOpenSSl.Text = e.Version; });
        }

        private void TabItem_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private async void Window_Closed(object sender, EventArgs e)
        {
            await Laba2.CloseServers(777);
        }

        private void MenuSettings_Click(object sender, RoutedEventArgs e)
        {
            var setting = new SettingWindow("settings.ini");
            setting.ShowDialog();
        }
    }
}
