﻿using OpenSSL.Core;
using OpenSSL.Crypto;
using System;
/**
 * Вариант 13	
 * Шифратор и дешифратор файлов ассиметричным алгоритмом RSA 1024 bits. На вход программы передается входной файл, ключ, флаг шифрования/дешифрования, выходной файл.
 */



namespace CLaba1
{
    public static class MyExtencions {
        public static byte[] GetBytes(this string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(this byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
    class Program
    {
        static private string mess = "one, two, three...";

        static void Main(string[] args)
        {
            try {
                RSA rsa = new RSA();
                rsa.GenerateKeys(1024, 65537, null, null);
                var res =    rsa.PublicEncrypt(mess.GetBytes(), RSA.Padding.PKCS1);
                var decmes = rsa.PrivateDecrypt(res, RSA.Padding.PKCS1).GetString();
            }
			catch (Exception e)
			{
                Console.WriteLine(e.InnerException.Message);
			}
}

        private static string OnPassword(bool verify, object userdata)
        {
            throw new NotImplementedException();
        }

        private static int callbackMethod(int p, int n, object arg)
        {
            return 400;
        }
    }
}
