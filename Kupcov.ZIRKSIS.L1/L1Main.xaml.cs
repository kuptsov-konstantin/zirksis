﻿using Kupcov.ZIRKSIS.Events;
using Microsoft.Win32;
using OpenSSL.Core;
using OpenSSL.Crypto;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Kupcov.ZIRKSIS.L1
{
   
    public class KeyPem
    {
        private string _file;
        private string _path;

        public KeyPem() { }
        public KeyPem(string path, string file)
        {
            _path = path;
            _file = file;
        }
        public static string PrivateKey(string path, string file)
        {
            return $"{path}\\{file}.private.pem";
        }
        public static string PublicKey(string path, string file)
        {
            return $"{path}\\{file}.public.pem";
        }
        public string PrivateKeyPEN { get { return PrivateKey(_path, _file); } }
        public string PublicKeyPEN { get { return PublicKey(_path, _file); } }
    }
    /// <summary>
    /// Логика взаимодействия для L1Main.xaml
    /// </summary>
    public partial class L1Main : UserControl
    {
        KeyPem keyPem;
        RSA rsa = new RSA();
        public string LabaName = "Лаба 1";
        string OpenFileNameForEncrypt { get; set; } = "";
        string SaveFileNameForEncrypt { get; set; } = "";
        string OpenFileNameForDecrypt { get; set; } = "";
        string SaveFileNameForDecrypt { get; set; } = "";
        public string KeyFileName { get; private set; }

        public event EventHandler<OpenSslVersionEventArgs> OpenSslVersion;

        DirectoryInfo
            OpenFilePathForEncrypt,
            SaveFilePathForEncrypt,
            OpenFilePathForDecrypt,
            SaveFilePathForDecrypt;

        private string latestBrowsRoot = Environment.CurrentDirectory;

        public L1Main()
        {
            InitializeComponent();
        
        }

        #region Шифровка
        private void OpenFileDlgForEncrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name + ".shif";
                oShifLable.Content = OpenFileNameForEncrypt;
                sShifLable.Content = SaveFileNameForEncrypt;
            }
        }
        private void SaveEncryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.DefaultExt = ".shif";
                myDialog.Filter = "RSA shif (.shif)|*.shif";
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForEncrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForEncrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForEncrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private async void Encrypt_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt == null)
            {
                MessageBox.Show($"Вы не указали файл для чтения!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = false; });
            Func<string> SaveFileFunc = () => {
                if (SaveFilePathForEncrypt == null)
                {
                    return OpenFilePathForEncrypt.FullName + ".shif";
                }
                else
                {
                    return SaveFilePathForEncrypt.FullName;
                }
            };
            using (FileStream file = File.Open(OpenFilePathForEncrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    if ((keyPem == null) || (rsa == null))
                    {
                        MessageBox.Show($"Нет ключей!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                        Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
                        return;
                    }
                    else
                    {
                        var publicKey = File.ReadAllText(keyPem.PublicKeyPEN);
                        CryptoKey key = CryptoKey.FromPublicKey(publicKey, "");
                        rsa = key.GetRSA();
                    }
                   
                    byte[] b = new byte[rsa.Size - 11 ];
                    while (await file.ReadAsync(b, 0, b.Length) > 0)
                    {
                        UTF8Encoding temp = new UTF8Encoding(true);
                        var res = rsa.PublicEncrypt(b, RSA.Padding.PKCS1);
                        await savefile.WriteAsync(res, 0, res.Length);
                        await savefile.FlushAsync();
                    }
                    savefile.Close();
                }
                file.Close();
                MessageBox.Show($"Файл зашифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
                

            }
            Encrypt.Dispatcher.Invoke(() => { Encrypt.IsEnabled = true; });
        }


        #endregion

        private void CreateKey_Click(object sender, RoutedEventArgs e)
        {
            using (System.Windows.Forms.FolderBrowserDialog folderBrowserDlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                folderBrowserDlg.SelectedPath =  latestBrowsRoot;
                System.Windows.Forms.DialogResult result = folderBrowserDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    keyPem = null;
                   
                    rsa.GenerateKeys(1024, 65537, null, null);

                    KeyFileName = string.Format("{0:X8}", (DateTime.Now + rsa.PrivateKeyAsPEM + rsa.PublicKeyAsPEM).GetHashCode());
                    latestBrowsRoot = folderBrowserDlg.SelectedPath;
                    File.WriteAllText(KeyPem.PrivateKey(latestBrowsRoot, KeyFileName), rsa.PrivateKeyAsPEM);
                    File.WriteAllText(KeyPem.PublicKey(latestBrowsRoot, KeyFileName), rsa.PublicKeyAsPEM);
                    MessageBox.Show($"Файлы ключей созданы! С именем {KeyFileName}.", "Создание ключей", MessageBoxButton.OK, MessageBoxImage.Information);
                    keyPair.Content = $"Пара №: {KeyFileName}";
                }

            }
        }

        private void OpenKey_Click(object sender, RoutedEventArgs e)
        {
            using (System.Windows.Forms.FolderBrowserDialog folderBrowserDlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                folderBrowserDlg.SelectedPath = latestBrowsRoot;
                System.Windows.Forms.DialogResult result = folderBrowserDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    latestBrowsRoot = folderBrowserDlg.SelectedPath;
                    var keysWindows = new KeyList(latestBrowsRoot);
                    keysWindows.PairChanged += LoadingKeysFromFile;
                    keysWindows.ShowDialog();
                    keyPair.Content = $"Пара №: {KeyFileName}";      
                }
            }
        }

        private void LoadingKeysFromFile(object sender, PairChangedEventArgs e)
        {
            KeyFileName = e.FileName;
            keyPem = new KeyPem(e.FilePath, e.FileName);           
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            OpenSslVersion?.Invoke(this, new OpenSslVersionEventArgs() { Version = OpenSSL.Core.Version.Library.ToString() });
        }




        #region Дешифровка
        private void SaveDecryptedFileDlg_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFilePathForEncrypt != null)
            {
                SaveFileDialog myDialog = new SaveFileDialog();
                myDialog.CheckFileExists = true;
                myDialog.FileName = SaveFilePathForEncrypt.FullName;
                if (myDialog.ShowDialog() == true)
                {
                    SaveFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                    SaveFileNameForDecrypt = OpenFilePathForEncrypt.Name;
                    sShifLable.Content = SaveFileNameForDecrypt;
                }
            }
            else
            {
                MessageBox.Show("Вы не выбрали файл для чтения!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenFileDlgForDecrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = true;
            myDialog.DefaultExt = ".shif";
            myDialog.Filter = "RSA shif (.shif)|*.shif";
            if (myDialog.ShowDialog() == true)
            {
                OpenFilePathForDecrypt = new DirectoryInfo(myDialog.FileName);
                OpenFileNameForDecrypt = OpenFilePathForDecrypt.Name;
                SaveFileNameForDecrypt = Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) + '\\'+ 
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                oShifFilePathLabel.Content = OpenFileNameForDecrypt;
                saveDeshFilePathLabel.Content = SaveFileNameForDecrypt;
            }
        }

        private async void Decrypt_Click(object sender, RoutedEventArgs e)
        {
            Func<string> SaveFileFunc = () => {
                if (SaveFilePathForDecrypt == null)
                {
                    return Path.GetDirectoryName(OpenFilePathForDecrypt.FullName) +
                    Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name)) + '\\' +
                    ".decrypt" + Path.GetExtension(Path.GetFileNameWithoutExtension(OpenFilePathForDecrypt.Name));
                }
                else
                {
                    return SaveFilePathForDecrypt.FullName;
                }
            };
            using (FileStream file = File.Open(OpenFilePathForDecrypt.FullName, FileMode.Open))
            {
                using (FileStream savefile = File.Create(SaveFileFunc.Invoke()))
                {
                    if (keyPem == null)
                    {
                        if (rsa == null)
                        {
                            MessageBox.Show($"Нет ключей!", "Статус", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                    else
                    {
                        var privateKey = File.ReadAllText(keyPem.PrivateKeyPEN);
                        CryptoKey key = CryptoKey.FromPrivateKey(privateKey, "");
                        rsa = key.GetRSA();
                    }
                    byte[] b = new byte[rsa.Size];
                    while (await file.ReadAsync(b, 0, b.Length) > 0)
                    {
                        UTF8Encoding temp = new UTF8Encoding(true);
                        var res = rsa.PrivateDecrypt(b, RSA.Padding.PKCS1);
                        await savefile.WriteAsync(res, 0, res.Length);
                        await savefile.FlushAsync();
                    }
                    savefile.Close();
                }
                file.Close();
                MessageBox.Show($"Файл дешифрован!", "Статус", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
