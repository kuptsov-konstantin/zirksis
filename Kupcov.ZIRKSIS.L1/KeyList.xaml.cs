﻿using System;
using System.Collections.Generic;
//using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kupcov.ZIRKSIS.L1
{
    /// <summary>
    /// Логика взаимодействия для KeyList.xaml
    /// </summary>
    public partial class KeyList : Window
    {
        private string _path;

        public event EventHandler<PairChangedEventArgs> PairChanged;
        public KeyList()
        {
            InitializeComponent();
        }

        public KeyList(string path) : this()
        {
            this._path = path;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxFiles.SelectedIndex > -1)
            {
                PairChanged(this, new PairChangedEventArgs()
                {
                    FileName = (string)listBoxFiles.SelectedValue,
                    FilePath = _path
                });
                this.Close();
            }
            else
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OkButton.IsEnabled = false;
            var res = System.IO.Directory.GetFiles(_path).Distinct();
            res =  res.Select(p => System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetFileNameWithoutExtension(p))).Distinct();        
            foreach (var item in res)
            {          
                listBoxFiles.Items.Add(item);
            }
        }

        private void listBoxFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OkButton.Dispatcher.Invoke(() => OkButton.IsEnabled = true);
        }
    }

    public class PairChangedEventArgs : EventArgs
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
